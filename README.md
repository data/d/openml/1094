# OpenML dataset: Acorns

https://www.openml.org/d/1094

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/Acorns.html

Acorn Size Oak Distribution

Reference:   Aizen and Patterson.  (1990). Journal of Biogeography, volume 17,  p. 327-332.
Authorization:   contact authors
Description:   Interest lies is the relationship between the size of the acorn and the geographic range of the oak tree species.  Note that the Quercus tomentella Engelm species in the California region grows only on the Channel Islands (total area 1014 sq. km) and the island of Guadalupe (total area 265 sq. km).  All other species grow on the Continental United States.
Number of cases:   39
Variable Names:

Species:   Latin name of the species
Region:   Atlantic or California region
Range:   The geographic area covered by the species in km2x100
Acorn_size:   Acorn size (cm3)
Tree_height:   Tree Height (m)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1094) of an [OpenML dataset](https://www.openml.org/d/1094). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1094/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1094/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1094/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

